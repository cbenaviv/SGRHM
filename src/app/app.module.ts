import { NgModule, ErrorHandler } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {HttpModule} from '@angular/http';


import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler, Form } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ListahuertasProvider } from '../providers/listahuertas/listahuertas';
import{FormBuilder,FormGroup,Validators} from '@angular/forms';
import { CargarvoluntarioProvider } from '../providers/cargarvoluntario/cargarvoluntario';
import { DetallehuertaPage } from '../pages/detallehuerta/detallehuerta';
import {MenuaccesoPage} from '../pages/menuacceso/menuacceso';
import {EntrevistaPage} from '../pages/entrevista/entrevista';
import { ListacurrententrevistaProvider } from '../providers/listacurrententrevista/listacurrententrevista';
import {InfoentrevistaPage} from '../pages/infoentrevista/infoentrevista';
import {ListadoentrevistadosPage} from '../pages/listadoentrevistados/listadoentrevistados';
import {ListadocategoriasPage} from '../pages/listadocategorias/listadocategorias';
import { ListarentrevistadosProvider } from '../providers/listarentrevistados/listarentrevistados';
import { ListarmiembrosProvider } from '../providers/listarmiembros/listarmiembros';
import {ListapreguntasPage} from '../pages/listapreguntas/listapreguntas';
import {InfopreguntaPage} from '../pages/infopregunta/infopregunta';
import {LoginPage} from '../pages/login/login';
import {ListahuerterosPage} from '../pages/listahuerteros/listahuerteros';
import {Geolocation} from '@ionic-native/geolocation';
import {Camera} from '@ionic-native/camera';
import { IonicImageViewerModule } from 'ionic-img-viewer';





@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    DetallehuertaPage,
    MenuaccesoPage,
    EntrevistaPage,
    InfoentrevistaPage,
    ListadoentrevistadosPage,
    ListadocategoriasPage,
    ListapreguntasPage,
    InfopreguntaPage,
    LoginPage,
    ListahuerterosPage
    


  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule,
    IonicImageViewerModule

   
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    DetallehuertaPage,
    MenuaccesoPage,
    EntrevistaPage,
    InfoentrevistaPage,
    ListadoentrevistadosPage,
    ListadocategoriasPage,
    ListapreguntasPage,
    InfopreguntaPage,
    LoginPage,
    ListahuerterosPage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ListahuertasProvider,
    CargarvoluntarioProvider,
    ListacurrententrevistaProvider,
    ListarentrevistadosProvider,
    ListarmiembrosProvider,
    Geolocation,
    Camera

    

  ]
})
export class AppModule {}
