import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ListacurrententrevistaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ListacurrententrevistaProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ListacurrententrevistaProvider Provider');
  }

  obtenerDatos()
  {
    return this.http.get('http://rhmapp.000webhostapp.com/GestionEntrevista/listadoEntrevista.php');
  }



}
