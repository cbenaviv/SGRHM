import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ListapreguntasPage} from '../listapreguntas/listapreguntas';

/**
 * Generated class for the ListadocategoriasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listadocategorias',
  templateUrl: 'listadocategorias.html',
})
export class ListadocategoriasPage {
   
  misCategorias=[];
  entrevista;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
       //Recibe el parametro
       //this.entrevista 
       this.entrevista=navParams.data.entrevista;



    this.misCategorias=[
       
      {
        codigo:5,
        titulo:"Equipo Colaborador",
        icono: "md-flower",
        codEntrevista:this.entrevista.idEntrevista
      },
      
      
      {
        codigo:1,
        titulo:"Descripcion y Caracterizacion de Huerta",
        icono: "md-flower",
        codEntrevista:this.entrevista.idEntrevista
        
       },
       {
           codigo:2,
           titulo:"Relaciones con la comunidad, Colectivos u organizaciones",
           icono: "md-flower",
           codEntrevista:this.entrevista.idEntrevista
       },
       
       {
            codigo:3,
            titulo:"Practicas de Siembra",
            icono: "md-flower",
            codEntrevista:this.entrevista.idEntrevista
       },
      
       {

           codigo:4,
           titulo:"Practica para el mantenimiento de la huerta",
           icono:"md-flower",
           codEntrevista:this.entrevista.idEntrevista

       }
      


    ]

    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListadocategoriasPage');
  }

  openNavDetailsPage(item)
  {
    this.navCtrl.push(ListapreguntasPage,{item:item});
  }


}
