import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListadocategoriasPage } from './listadocategorias';

@NgModule({
  declarations: [
    ListadocategoriasPage,
  ],
  imports: [
    IonicPageModule.forChild(ListadocategoriasPage),
  ],
})
export class ListadocategoriasPageModule {}
