import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfoentrevistaPage } from './infoentrevista';

@NgModule({
  declarations: [
    InfoentrevistaPage,
  ],
  imports: [
    IonicPageModule.forChild(InfoentrevistaPage),
  ],
})
export class InfoentrevistaPageModule {}
