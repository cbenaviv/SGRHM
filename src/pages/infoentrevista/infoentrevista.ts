import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ListadoentrevistadosPage} from '../listadoentrevistados/listadoentrevistados';
import {ListadocategoriasPage} from '../listadocategorias/listadocategorias';
import { Http,Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

/**
 * Generated class for the InfoentrevistaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-infoentrevista',
  templateUrl: 'infoentrevista.html',
})
export class InfoentrevistaPage {
  entrevista;
  dato;
  vector=[];
  j;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http:HttpClient) {
    this.entrevista=navParams.data.entrevista;
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad InfoentrevistaPage');
   //Cargar la informacion de los  los entrevistados
   this.dato=[
     {
       "operacion":4,
       "Descripcion":"Listar Huerta Particular",
       "CodigoHuerta":this.entrevista.Huerta.IdHuerta
     },

  

   ]

   //alert(JSON.stringify(this.dato));

   let header:any =new HttpHeaders({'Content-Type':'application/json'});
   let url='http://rhmapp.000webhostapp.com/GestionHuerta/controllerHuerta.php';
   this.http.post(url,JSON.stringify(this.dato),header).
   subscribe(    
    (data):any=>{

      for(this.j in data )
      {
       this.vector.push(data[this.j].nombres+" "+data[this.j].apellidos);  
       
      }

    }
   )

  }

  ListarEntrevidados(entrevista)
  {
    
     this.navCtrl.push(ListadoentrevistadosPage,{entrevista:entrevista});
  }

  listarCategorias(entrevista)
  {
    this.navCtrl.push(ListadocategoriasPage,{entrevista:entrevista});
  }



}
