import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {ListahuertasProvider } from '../../providers/listahuertas/listahuertas';
import { DetallehuertaPage} from '../detallehuerta/detallehuerta';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

 huertas;
  constructor(public navCtrl: NavController,public provedorLista:ListahuertasProvider) {

  }

  ionViewDidLoad()
  {

    this.provedorLista.obtenerDatos().subscribe(
      (data)=>{this.huertas=data;},
      (error)=>{console.log(error);}
      
    );
  }

  openNavDetails(huerta)
  {
     this.navCtrl.push(DetallehuertaPage,{huerta:huerta});
  }
  


}
