import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuaccesoPage } from './menuacceso';

@NgModule({
  declarations: [
    MenuaccesoPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuaccesoPage),
  ],
})
export class MenuaccesoPageModule {}
