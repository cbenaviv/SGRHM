import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {TabsPage} from '../tabs/tabs';
import {EntrevistaPage} from '../entrevista/entrevista';
import {LoginPage} from '../login/login';


/**
 * Generated class for the MenuaccesoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menuacceso',
  templateUrl: 'menuacceso.html',
})
export class MenuaccesoPage {
  
  item;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.item=this.navParams.get("item");
    //alert(this.item.nombres+this.item.idUser);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuaccesoPage');
  }


  gestionarEntrevista()
  {
     this.navCtrl.setRoot(EntrevistaPage,{item:this.item});
  }


  gestionarHuertas()
  {
    this.navCtrl.setRoot(TabsPage,{item:this.item});
  }


  Salir()
  {
    this.navCtrl.setRoot(LoginPage);
  }

}
