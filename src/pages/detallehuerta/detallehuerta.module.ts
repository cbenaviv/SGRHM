import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetallehuertaPage } from './detallehuerta';

@NgModule({
  declarations: [
    DetallehuertaPage,
  ],
  imports: [
    IonicPageModule.forChild(DetallehuertaPage),
  ],
})
export class DetallehuertaPageModule {}
