import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ListahuerterosPage} from '../listahuerteros/listahuerteros';

/**
 * Generated class for the DetallehuertaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detallehuerta',
  templateUrl: 'detallehuerta.html',
})
export class DetallehuertaPage {
  huerta;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.huerta=navParams.data.huerta;
    //alert(JSON.stringify(this.huerta));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetallehuertaPage');
  }

  ListarHuerteros(Codigo)
  {
    this.navCtrl.push(ListahuerterosPage,{huerta:Codigo});

  }

}
