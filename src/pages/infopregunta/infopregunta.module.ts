import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfopreguntaPage } from './infopregunta';

@NgModule({
  declarations: [
    InfopreguntaPage,
  ],
  imports: [
    IonicPageModule.forChild(InfopreguntaPage),
  ],
})
export class InfopreguntaPageModule {}
