import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {FormBuilder,FormGroup,Validators,FormControl} from '@angular/forms';
import { Jsonp } from '@angular/http';
import { ListapreguntasPage } from '../listapreguntas/listapreguntas';

/**
 * Generated class for the InfopreguntaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-infopregunta',
  templateUrl: 'infopregunta.html',
})
export class InfopreguntaPage {
  pregunta;
  question;
  descripcionOpcion;
  respuesta;
  tipoPregunta;
  esAbierta;
  listaOpciones;
  esUnica;
  FormRespuestaAbierta:FormGroup;
  FormRespuestaCerradaUnica:FormGroup;
  FormRespuestaCerradaMultiple:FormGroup;
  detailsForm:FormGroup;
  FormAuxiliar:FormGroup;
  FormEdicionAbierta:FormGroup;
  FormEdicionCerradaUnica:FormGroup;
  FormEdicionCerradaMultiple:FormGroup;
  answer:any[] = [];  
  listado;
  respuestaRegistradaAbierta;
  vector=[];
  opcionesCargadas;
  opcionesDefault;
  vectorSelector=[];
  EsOtro;
  otroConte;
  globalIdOpcionRespuesta;
  litterVector=[];
  idOpcioOtro;
  solution;
  opcionRespuesta;
  dataTmp;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,public http:HttpClient,
    private fb:FormBuilder) {
    this.pregunta=navParams.data.question;
    this.listado=navParams.data.pregunta;
    
    this.FormAuxiliar=this.fb.group(
      {
        arbeite:['',Validators.required]
      }
    );

    this.cargarFormulariEdicionAbierta();

  
    
  
    

      
    
  }




  
  ionViewDidLoad() {
   
    //this.cargarFormulariEdicionAbierta();
    //this.cargarFormulariEdicionAbierta();
    this.cargarFormularioEdicionCerradaUnica();
    this.cargarFormularioEdicionRespuestaMultiple();
    

    let dato=[

      {  "operacion":1,//Verifica si el user ya tiene registrada respuesta
         "idPregunta":this.pregunta.CodPregunta,
         "idEntrevista":this.pregunta.CodEntrevista

      },
    ];


    
    let header:any =new HttpHeaders({'Content-Type':'application/json'});
    let url='http://rhmapp.000webhostapp.com/GestionRespuesta/controllerRespuesta.php';
    this.http.post(url,JSON.stringify(dato),header).subscribe(
      (data):any=>{this.question=data 
        //alert(JSON.stringify(this.question));
        this.respuesta=data[0].Respuesta;//Validemos si Existe Respuesta
        this.tipoPregunta=data[0].TipoPregunta;
        this.listaOpciones=data[0].listado;
        alert(JSON.stringify(this.listaOpciones));
        this.opcionRespuesta=data[0].OpcionRespuesta;
        //Json  
        if(this.respuesta=="true")
      {
         this.respuesta=true;
         
         if(this.tipoPregunta=="Abierta")
         {
           
                   
          this.esAbierta=true;
          this.dataTmp=this.listaOpciones[0].ContenidoAuxiliar;
           
           
         }
         else
         { /// Seleccion

          this.esAbierta=false;
          //this.cargarOpcionesDefaultdos(this.pregunta.CodPregunta,this.pregunta.CodEntrevista);

          if(this.opcionRespuesta=="Unica")
          {
                this.cargarFormularioEdicionCerradaUnica();
          }

          else
          {   alert("Carga Multiple");
              this.cargarOpcionesDefault(this.pregunta.CodPregunta,this.pregunta.CodEntrevista);
             
                this.cargarFormularioEdicionRespuestaMultiple();
                //alert(JSON.stringify(this.listaOpciones));

                for(let entry of this.listaOpciones)
                {
                  let answer=this.multipleCheck(entry.idOpcionRespuesta);
                  //alert(entry.idOpcionRespuesta);                
                 
  
  
                }
  
  


              
              
              


          }


         }

         

         

      }
      else
      {
         this.respuesta=false;
         //Cargue la Opcion Por Defecto
         this.cargarOpcionesDefault(this.pregunta.CodPregunta,this.pregunta.CodEntrevista);

         if(this.tipoPregunta=="Abierta")
         {
            //No tiene Respuesta y la pregunta es abierta
            this.esAbierta=true;
            this.cargarFormularioRespuestaAbierta();
         }
         else
         {
            //No tiene Respuesta y la pregunta Es cerrada
            this.esAbierta=false;
            //Validemos si es Cerrada O Abierta
            if(this.opcionRespuesta=="Unica")
            { 
              //Respuesta Cerrada Unica
              this.esUnica=true;
              this.cargarFormularioRespuestaCerradaUnica();
            }
            else
            {
              //Respuesta Cerrada Multiple
              this.esUnica=false;
              this.cargarFormularioRespuestaCerradaMultiple();
              
              alert(JSON.stringify(this.vectorSelector))
              
            }



         }
     

      }  
        
        














      });

     

      




    
   
  }

  cargarFormulariEdicionAbierta()
  {
    this.FormEdicionAbierta=this.fb.group(

      {
         conteRespuesta:['',Validators.required]
      }
     )
  }


  cargarFormularioEdicionCerradaUnica()
  {
    
    
let porDefecto;
if(this.respuesta==false)
{
  this.FormEdicionCerradaUnica=this.fb.group(
    {
      opcionRespuesta:[porDefecto],
      campoOtro:['']
    }
  )
} 
else
{  
  //let porDefecto;  
  for(let ck of this.vectorSelector)
    {
       if(ck.isSelected==true)
       {
            porDefecto=ck.idOpcionRespuesta;
            //porDefecto.push(ck.idOpcionRespuesta);
       }
    }


    this.FormEdicionCerradaUnica=this.fb.group(
      {
        opcionRespuesta:[porDefecto],
         campoOtro:['']
      }
    )

} 
   
  }


  cargarFormularioEdicionRespuestaMultiple()
  {   
       let data=this.cargarOpcionesMultipleRegistrada();
       
        this.FormEdicionCerradaMultiple=this.fb.group(
          {
            arbeite:[data,Validators.required],
            Otro:['']
          }
        )
      
      
        


      
      
    
    
  }

  cargarOpcionesMultipleRegistrada()
  {
   }


  cargarOpcionesDefault(codPregunta,codEntrevista)
  {
     //Metodo que carga las opciones que deben listar a la pregunta
    let lista=[
      {
        "operacion":10,
        "Descripcion":"Se listara todas la opciones de Respuesta",
         "CodigoPregunta":codPregunta,
         "CodigoEntrevista":codEntrevista
      }
    ];

    let header:any =new HttpHeaders({'Content-Type':'application/json'});
    let url='http://rhmapp.000webhostapp.com/GestionRespuesta/controllerRespuesta.php';
    this.http.post(url,JSON.stringify(lista),header).subscribe(
      (data):any=>{this.opcionesDefault=data
       
      }
    );

    



  }

  cargarOpcionesDefaultdos(codPregunta,codEntrevista)
  {
      this.cargarOpcionesDefault(codPregunta,codEntrevista);      
      this.selector();
      
      
  }




  selector()
  {
     
   //Metodo que carga las seleccione tanto de respuesta cerrada unica como multiple

   for(let entry of this.opcionesDefault)
   {
     //idOpcionRespuesta; entry.idOpcionRespuesta
      if(this.esUnica)
      {
        this.selectorCheck(entry.idOpcionRespuesta,entry.DescripcionRespuesta);
        

      }
      else{
        
           let answer=this.multipleCheck(entry.idOpcionRespuesta);
           let obj=[
             {
              "idOpcionRespuesta":entry.idOpcionRespuesta,
              "DescripcionRes":entry.DescripcionRespuesta,
              "isSelected":answer

             }
           ];
           
           //f.localeCompare(k)===0||f.localeCompare(q)==0
            var miller:string=entry.DescripcionRespuesta;
           if(answer==true&&miller.localeCompare("Otras")==0)//Otro
           {
            
            this.EsOtro=true;
           }
           else{
             this.EsOtro=false;
           }

           this.vectorSelector.push(obj);


      }
     
    
     

   }


  }

  //Error--->Esta Aqui
   multipleCheck(idOpcionRespuesta)
   {
       alert(JSON.stringify(this.opcionesDefault)+"Algo")
    for(let entry of this.opcionesDefault[0])
    {
       if(entry.idOpcionRespuesta==idOpcionRespuesta)
       {
           return true;
           
       }
    }

    return false;



   }



  selectorCheck(idOpcionRespuesta,DescripcionOpcionRespuesta)
  { 
    let opcion_verfi=[];
    for(let entry of this.listaOpciones)
    {   
        
     
     
     
     if(entry.idOpcionRespuesta==idOpcionRespuesta)
        {
          //Check Elemet
          opcion_verfi=[
             {
               "idOpcionRespuesta":idOpcionRespuesta,
               "DescripcionRes":DescripcionOpcionRespuesta,
               "isSelected":true

             }
          ]

             var pencil:string="Otra(s)";
             var brauche:string="DescripcionOpcionRespuesta";
             //conteAux.localeCompare(tmpe)

            if(brauche.localeCompare(pencil))
            {
                this.EsOtro=true;
                this.otroConte=entry.DescripcionOpcionRespuesta;
            }

        }

        else{
          //No Check Elment
          opcion_verfi=[
           {
             "idOpcionRespuesta":idOpcionRespuesta,
             "DescripcionRes":DescripcionOpcionRespuesta,
             "isSelected":false

           }
        ]


        }
       

        this.vectorSelector.push(opcion_verfi);
    }

  }

 

  cargarFormularioRespuestaAbierta()
  {
     this.FormRespuestaAbierta=this.fb.group(

      {
         detalleRespuesta:['',Validators.required]
      }
     )
  }

  cargarFormularioRespuestaCerradaUnica()
  {  
      this.FormRespuestaCerradaUnica=this.fb.group(
        {
          opcionAnswer:['',Validators.required],
          Otro:['']
        }
      );
  }

  cargarFormularioRespuestaCerradaMultiple()
  {  
    
   
    
    this.FormAuxiliar=this.fb.group(
      {
        arbeite:['',Validators.required],
        Otro:['']
      }
    );



  }

 

  guardarRespuestaAbierta()
  {
     let respuesta=[
    {
   "operacion":4,
   "Descripcion":"Insertar Respuesta Abierta",
   "contenidoRespuesta":this.FormRespuestaAbierta.value.detalleRespuesta,
   "codigoPregunta":this.pregunta.CodPregunta,
   "codigoEntrevista":this.pregunta.CodEntrevista,
   "idPreguntaPorEntrevista":this.question[0].idPreguntaPorEntrevista,
   "idOpcionRespuesta":this.listaOpciones[0].idOpcionRespuesta

    }
   ];
    
   //alert(JSON.stringify(respuesta));
   
    let header:any =new HttpHeaders({'Content-Type':'application/json'});
    let url='http://rhmapp.000webhostapp.com/GestionRespuesta/controllerRespuesta.php';
    this.http.post(url,JSON.stringify(respuesta),header).subscribe(    
      (data):any=>{
  
        //alert(JSON.stringify(data));
  
      }
     )
     

    this.navCtrl.pop();
     
  
   
    


  }

  guardarRespuestaCerradaUnica()
  {
    //Se presentara la situacio de que otro este marcado o no
    if(this.EsOtro==false)
    {
      let respuesta=[
        {
         "operacion":5,
         "Descripcion":"Ingreso respuesta Cerrada Unica",
         "codigoPregunta":this.pregunta.CodPregunta,
         "codigoEntrevista":this.pregunta.CodEntrevista,
         "idPreguntaPorEntrevista":this.question[0].idPreguntaPorEntrevista,
         "idOpcionRespuesta":this.FormRespuestaCerradaUnica.value.opcionAnswer
   
        }
   
   
   
      ]
   
       //alert(JSON.stringify(respuesta));
       let header:any =new HttpHeaders({'Content-Type':'application/json'});
       let url='http://rhmapp.000webhostapp.com/GestionRespuesta/controllerRespuesta.php';
       this.http.post(url,JSON.stringify(respuesta),header).subscribe(    
         (data):any=>{
     
           //alert(JSON.stringify(data));
     
         }
        )
       
        this.navCtrl.pop();
      
    }
    else{
      //Existe Otro
      let respuesta=[
        {
       "operacion":4,
       "Descripcion":"Insertar Respuesta Abierta",
       "contenidoRespuesta":this.FormRespuestaCerradaUnica.value.Otro,
       "codigoPregunta":this.pregunta.CodPregunta,
       "codigoEntrevista":this.pregunta.CodEntrevista,
       "idPreguntaPorEntrevista":this.question[0].idPreguntaPorEntrevista,
       "idOpcionRespuesta":this.FormRespuestaCerradaUnica.value.opcionRespuesta
    
        }
       ];

       //alert(JSON.stringify(respuesta));

       let header:any =new HttpHeaders({'Content-Type':'application/json'});
      let url='http://rhmapp.000webhostapp.com/GestionRespuesta/controllerRespuesta.php';
      this.http.post(url,JSON.stringify(respuesta),header).subscribe(    
      (data):any=>{
  
        //alert(JSON.stringify(data));
  
      }
     )
     



     
     this.navCtrl.pop();
     
      
    }
    
  }

  guardarCerraMultiple()
  {
    this.vector=[];
    let objeto=[];
    let i:number=0;
    for(let k in this.opcionesCargadas)
    {
         objeto=[
           {
              
              "opcionRespuesta":this.opcionesCargadas[k]

           }
         ]

         this.vector.push(objeto);
         i++;
    }

     
        let Encabezado=[
          {
            "operacion":6,
             "Descripcion":"Ingreso de Respuesta Cerrada Multiple",
             "codigoPregunta":this.pregunta.CodPregunta,
             "codigoEntrevista":this.pregunta.CodEntrevista,
             "idPreguntaPorEntrevista":this.question[0].idPreguntaPorEntrevista,
             "listado":this.vector,
             "numeroRegistros":i
  
          }
  
        ]
  
        //alert(JSON.stringify(Encabezado));
  
      let header:any =new HttpHeaders({'Content-Type':'application/json'});
      let url='http://rhmapp.000webhostapp.com/GestionRespuesta/controllerRespuesta.php';
      this.http.post(url,JSON.stringify(Encabezado),header).subscribe(    
        (data):any=>{
    
          //alert(JSON.stringify(data));
    
        }
       )

       
       if(this.EsOtro==true)
       {
           let dat=[
             {
               "operacion":11,
               "Descripcion":"Actualice Repuesta Registrada",
               "contenidoRespuesta":this.FormAuxiliar.value.Otro,
               "codigoPregunta":this.pregunta.CodPregunta,
               "idPreguntaPorEntrevista":this.question[0].idPreguntaPorEntrevista,
               "idOpcionRespuesta":this.globalIdOpcionRespuesta
   
               
              }
           ]

         alert(JSON.stringify(dat));
         let header:any =new HttpHeaders({'Content-Type':'application/json'});
         let url='http://rhmapp.000webhostapp.com/GestionRespuesta/controllerRespuesta.php';
         this.http.post(url,JSON.stringify(dat),header).subscribe(    
          (data):any=>{
      
            alert(JSON.stringify(data));
      
          }
         )
  
        

       }
  
       this.navCtrl.pop();
      
      

     

}


  oncambio(parametro)
  {
    
    this.EsOtro=false;
    this.opcionesCargadas=parametro;
    
  
    for(let entry in this.opcionesCargadas)
    {
        this.equivalenciaTexto(this.opcionesCargadas[entry]);
        //alert(this.opcionesCargadas[entry]);
    }
   
    
  }

  equivalenciaTexto(entry)
  {
     
    for(let us of this.opcionesDefault)
    {
      if(us.idOpcionRespuesta==entry)
      {
         var j:string=us.DescripcionRespuesta;
         var k:string="Otro(s)"
          //alert(j);
       
       // if(us.DescripcionRespuesta=="Otra(s)"|| us.DescripcionRespuesta=="Otra(s)")
         if(j.localeCompare(k)==0)
        {
          this.EsOtro=true;
          //alert("Selecciono Otro");
          this.globalIdOpcionRespuesta=entry;
        }
        
      }
      
      
    }
    
  }

  checkOtro(respuesta)
  {
       
        this.solution=respuesta.idOpcionRespuesta;
        //respuesta.DescripcionOpcionRespuesta
        var f: string = "Otro(s)";
        var k:string=respuesta.DescripcionRespuesta;
        var q:string=respuesta.DescripcionRes;
       
       

       

        //respuesta.DescripcionRes==="Otro(s)"
      if( f.localeCompare(k)===0||f.localeCompare(q)==0)
      {
        this.EsOtro=true;
        //alert("Entro Otro");
      }
      else{
        this.EsOtro=false;
        //alert("Keep Normal");
      }
  }

  cargarOpcionesDefaultwo(codPregunta,codEntrevista)
  {
     
    let lista=[
      {
        "operacion":10,
        "Descripcion":"Se listara todas la opciones de Respuesta",
         "CodigoPregunta":codPregunta,
         "CodigoEntrevista":codEntrevista
      }
    ];

    let header:any =new HttpHeaders({'Content-Type':'application/json'});
    let url='http://rhmapp.000webhostapp.com/GestionRespuesta/controllerRespuesta.php';
    this.http.post(url,JSON.stringify(lista),header).subscribe(
      (data):any=>{this.opcionesDefault=data
      //alert(JSON.stringify(this.opcionesDefault));
      
      }
    );
   



  }


  ActualizarRespuestaAbierta()
  {
    //Actualiza la respuesta Abierta
    let data=[
      {
        "operacion":11,
        "Descripcion":"Actualice Respuesta Abierta",
        "codPregunta":this.pregunta.CodPregunta,
        "idPreguntaPorEntrevista":this.question[0].idPreguntaPorEntrevista,
        "idOpcionRespuesta":this.listaOpciones[0].idOpcionRespuesta,
        "contenido":this.FormEdicionAbierta.value.conteRespuesta
        

      }
    ];
    //alert(data);
    let header:any =new HttpHeaders({'Content-Type':'application/json'});
    let url='http://rhmapp.000webhostapp.com/GestionRespuesta/controllerRespuesta.php';
    this.http.post(url,JSON.stringify(data),header).subscribe(
      (data):any=>{
      //alert(JSON.stringify(data));
      
      }
    );
   

    this.navCtrl.pop();
   


  }

  ActualizarRespuestaCerradaUnica()
  {
    //Existen dos Posibilidades, con Otro, Sin Otro
    //this.cargarFormularioEdicionCerradaUnica();
   let tmp=0;
   
      
      
      //alert(JSON.stringify(this.vectorSelector));
      let ant=this.loadSelectUnity(this.vectorSelector);
      if(ant!=null)
      {
        tmp=ant;
      }
      
    
   
     if(this.EsOtro)
     {
        //Otro
        let data=[
          {
            "operacion":12,
            "Descripcion":"Actualizar Respuesta Cerrada Con Otro",
            "codPregunta":this.pregunta.CodPregunta,
            "idPreguntaPorEntrevista":this.question[0].idPreguntaPorEntrevista,
            //"idOpcionRespuesta":tmp,
            "idOpcionRespuesta":this.FormEdicionCerradaUnica.value.opcionRespuesta,
            "ContenidoOtro":this.FormEdicionCerradaUnica.value.campoOtro
          }
        ];

        //alert(JSON.stringify(data));
        let header:any =new HttpHeaders({'Content-Type':'application/json'});
        let url='http://rhmapp.000webhostapp.com/GestionRespuesta/controllerRespuesta.php';
        this.http.post(url,JSON.stringify(data),header).subscribe(
          (data):any=>{
          //alert(JSON.stringify(data));
          
          }
        );

        this.navCtrl.pop();
        
       
     }
     else{
       //Sin Otro
       let data=[
         {
           "operacion":13,
           "Descripcion":"Actualizar Respuesta Cerrada Sin  Otro",
           "codPregunta":this.pregunta.CodPregunta,
           "idPreguntaPorEntrevista":this.question[0].idPreguntaPorEntrevista,
           "idOpcionRespuesta":this.FormEdicionCerradaUnica.value.opcionRespuesta,
           
           
         }
       ];

        //alert(JSON.stringify(data));

    let header:any =new HttpHeaders({'Content-Type':'application/json'});
    let url='http://rhmapp.000webhostapp.com/GestionRespuesta/controllerRespuesta.php';
    this.http.post(url,JSON.stringify(data),header).subscribe(
      (data):any=>{
      //alert(JSON.stringify(data));
      
      }
    );
    
    this.navCtrl.pop();


     }


  }

  ActualizarRespuestaCerradaMultiple()
  {
      //Existen Dos Posibilidades, Con Otro Sin Otro

      if(this.EsOtro)
      {
        let tmp=0; 
        let r=this.identifyOtras(this.vectorSelector);
         
         if(r!=null)
         {
            tmp=r;
         }
        
        let data=[
           {
             "operacion":14,
             "Descripcion":"Actualice Respuesta Cerrada Multiple Con Otro",
             "codPregunta":this.pregunta.CodPregunta,
             "idPreguntaPorEntrevista":this.question[0].idPreguntaPorEntrevista,
             "contenido":this.FormEdicionCerradaMultiple.value.Otro,
             "idOpcioOtro":tmp,
             //"listaOpciones":this.FormEdicionCerradaMultiple.value.arbeite
             "ListaOption":this.cargarJsonSeleccion(this.FormEdicionCerradaMultiple.value.arbeite)
             
             
           },

         ];
        //alert(JSON.stringify(data));

        let header:any =new HttpHeaders({'Content-Type':'application/json'});
        let url='http://rhmapp.000webhostapp.com/GestionRespuesta/controllerRespuesta.php';
        this.http.post(url,JSON.stringify(data),header).subscribe(
          (data):any=>{
          //alert(JSON.stringify(data));
          
          }
        );
       

      }
      else
      {
         let data=[
           {
             "operacion":15,
             "Descripcion":"Actualice Respuesta Cerrada Multiple Sin Otro",
             "codPregunta":this.pregunta.CodPregunta,
             "idPreguntaPorEntrevista":this.question[0].idPreguntaPorEntrevista,
             //"listaOpciones":this.FormEdicionCerradaMultiple.value.arbeite
             "listaOpciones":this.cargarJsonSeleccion(this.FormEdicionCerradaMultiple.value.arbeite)
             
             
           }
         ];
         //alert(JSON.stringify(data));
         //this.cargarJsonSeleccion(this.FormEdicionCerradaMultiple.value.arbeite);

    let header:any =new HttpHeaders({'Content-Type':'application/json'});
    let url='http://rhmapp.000webhostapp.com/GestionRespuesta/controllerRespuesta.php';
    this.http.post(url,JSON.stringify(data),header).subscribe(
      (data):any=>{
      //alert(JSON.stringify(data));
      
      }
    );
         
    this.navCtrl.pop();
      }

  }


  cargarJsonSeleccion(seleccion)
  {
    let helpvector=[];
    let objeto=[];
    let i:number=0;
    for(let k in seleccion)
    {
         objeto=[
           {
              
              "opcionRespuesta":seleccion[k]

           }
         ]

         helpvector.push(objeto);
         i++;
    }
    
    //alert(JSON.stringify(helpvector));
    return helpvector;
     
       
    
  }


  

  loadSelectUnity(vector)
  {
    for(let k of vector)
    {
      if(k[0].isSelected==true)
      {
        return k[0].idOpcionRespuesta;
      }
    }

    return null;
  }

  identifyOtras(vector)
  {
    for(let k of vector)
    {
      if(k[0].DescripcionRes=="Otras")
      {
        return k[0].idOpcionRespuesta;
      }
    }

    return null;
  }



  

 

  
  



}
