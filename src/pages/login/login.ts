import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController} from 'ionic-angular';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Jsonp } from '@angular/http';
import {MenuaccesoPage} from '../menuacceso/menuacceso';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  FormLogin:FormGroup;
  j;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public http:HttpClient,private fb:FormBuilder,private alertControl:AlertController) {
      this.crearFormulario();
  }

  ionViewDidLoad() {
  
  }

  crearFormulario()
  {
     this.FormLogin=this.fb.group(
       {
          usuario:['',Validators.required],
          password:['',Validators.required]
       }
     )
  }

  validarUser()
  {
     let user=[
      {
         "operacion":0,
         "Descripcion":"Verifique Username",
         "username":this.FormLogin.value.usuario,
         "password":this.FormLogin.value.password
      },

     ]
    
   let header:any =new HttpHeaders({'Content-Type':'application/json'});
   let url='http://rhmapp.000webhostapp.com/GestionIngreso/controllerIngreso.php';
   this.http.post(url,JSON.stringify(user),header).
   subscribe(    
    (data):any=>{this.j=data

         //alert(JSON.stringify(this.j));
         if(this.j[0].Respuesta=="Exito")
         {
          this.navCtrl.setRoot(MenuaccesoPage,{item:this.j[0]});
         }
         else{

          let alert=this.alertControl.create(
            {
              title:'Validación de Usuario',
              subTitle:'Usuario No Registrado',
              buttons:['ok']
            }
          );
      
          alert.present();


         }

    }
   );
  
        
    
  }

}
