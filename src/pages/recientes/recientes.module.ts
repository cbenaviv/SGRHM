import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecientesPage } from './recientes';

@NgModule({
  declarations: [
    RecientesPage,
  ],
  imports: [
    IonicPageModule.forChild(RecientesPage),
  ],
})
export class RecientesPageModule {}
