import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ListacurrententrevistaProvider} from '../../providers/listacurrententrevista/listacurrententrevista';
import {InfoentrevistaPage} from '../infoentrevista/infoentrevista';

/**
 * Generated class for the RecientesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recientes',
  templateUrl: 'recientes.html',
})
export class RecientesPage {
  entrevistas;
  constructor(public navCtrl: NavController, public navParams: NavParams,public provedorInterviewCurrent:ListacurrententrevistaProvider) {
    //alert(this.item.nombres+this.item.idUser);
 
  }

 
  ionViewDidLoad()
  {

    this.provedorInterviewCurrent.obtenerDatos().subscribe(
      (data)=>{this.entrevistas=data;},
      (error)=>{console.log(error);}
      
    );
  }

  openNavDetailsPage(entrevista)
  {
     this.navCtrl.push(InfoentrevistaPage,{entrevista:entrevista})
  }

}
