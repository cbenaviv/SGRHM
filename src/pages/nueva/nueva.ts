import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { ListahuertasProvider } from '../../providers/listahuertas/listahuertas';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';
import 'rxjs/add/operator/map';
import { Jsonp } from '@angular/http';

/**
 * Generated class for the NuevaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nueva',
  templateUrl: 'nueva.html',
})
export class NuevaPage {
  huertas;
  dato;
  j;
  componente=[];
  FormEntrevista:FormGroup;
  vectorObjetos=[];
  vector=[];
  entrevistados;
  version;
  versionData;
  versionNumber:number;
  investigadores;
  valorHuerta;
  constructor(public navCtrl: NavController, public navParams: NavParams,public provedorLista:ListahuertasProvider,
  public http:HttpClient,private fb:FormBuilder,private alertControl:AlertController) {

    this.crearFormulario();
  }

  crearFormulario()
  {   
      this.FormEntrevista=this.fb.group(
        {
          huerta:['',Validators.required],
          fecha:['',Validators.required],
          investigador:['',Validators.required],
          Entrevistados:['',Validators.required],
          Version:['',Validators.required],
          Observaciones:['',Validators.required]
        }
      )
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad NuevaPage');
    this.provedorLista.obtenerDatos().subscribe(
      (data)=>{this.huertas=data;},
      (error)=>{console.log(error);}
      
    )

    //Metodo que me carga la lista de investigadores
    this.listaInvestigadores();
  }

 listaInvestigadores()
 {
    
   let dato=[
   {
     "operacion":1,
     "Descripcion":"Liste los investigadores"

   }

   ]
    
   let header:any =new HttpHeaders({'Content-Type':'application/json'});
   let url='http://rhmapp.000webhostapp.com/GestionIngreso/listaEntrevistadores.php';

   this.http.post(url,JSON.stringify(this.dato),header).
   subscribe(    
    (data):any=>{this.investigadores=data

      //alert(JSON.stringify(this.investigadores));

    }
   );


   
   



  
 }


  onChange(valor)
  {
   
    this.valorHuerta=valor;
    this.dato=[
      {
        "operacion":4,
        "Descripcion":"Listar Huerta Particular",
        "CodigoHuerta":valor
      }
      
   
 
    ];
  
  
   let header:any =new HttpHeaders({'Content-Type':'application/json'});
   let url='http://rhmapp.000webhostapp.com/GestionHuerta/controllerHuerta.php';
   this.http.post(url,JSON.stringify(this.dato),header).
   subscribe(    
    (data):any=>{this.j=data

      

    }
   );

   //Cargue la Version de la huerta
   this.version=[
    {
      "operacion":5,
      "Descripcion":"Cargar Version",
      "CodigoHuerta":valor
    }

   ];
  this.cargarVersion();

  }

  cargarVersion()
  {
    
    let header:any =new HttpHeaders({'Content-Type':'application/json'});
    let url='http://rhmapp.000webhostapp.com/GestionHuerta/controllerHuerta.php';
    this.http.post(url,JSON.stringify(this.version),header).
   subscribe(    
    (data):any=>{this.versionData=data

     this.versionNumber=parseInt(data[0].Version)+1;
     
    }
   )


  }


  guardarEntrevista()
  {
    let datirris=[
      {
         "operacion":4,
         "Descripcion":"Insertar Entrevista",
         "CodigoHuerta":this.FormEntrevista.value.huerta,
         "FechaEntrevista":this.FormEntrevista.value.fecha,
         "Investigador":this.FormEntrevista.value.investigador, //Enviar es el codigo del investigador
         "VersionEntrevista":this.FormEntrevista.value.Version,
         "Observaciones":this.FormEntrevista.value.Observaciones,
         "Entrevistados":this.FormEntrevista.value.Entrevistados


      }

    ]
     //alert(JSON.stringify(datirris));
     let header:any =new HttpHeaders({'Content-Type':'application/json'});
     let url='http://rhmapp.000webhostapp.com/GestionEntrevista/ControllerEntrevista.php';
     this.http.post(url,JSON.stringify(datirris),header).
    subscribe(    
     (data):any=>{
 
       if(data[0].Resultado=="Exito")
       {
         this.guardarEntrevisPorMiembro(data[0].CodigoEntrevista);
       }
 
     }
    )

    


  
  }


  oncambio(parametro)
  {
     //alert(parametro);
    this.entrevistados=parametro;
   
    
  }

  guardarEntrevisPorMiembro(CodigoEntrevista)
  {
    
    let objeto=[];

     



      for(let k in this.entrevistados)
      {
           objeto=[
             {
                "Codigo":CodigoEntrevista,
                "Miembro":this.entrevistados[k]

             }
           ]

           this.vector.push(objeto);
      }


      let Encabezado=[
        {
            "operacion":5,
            "Descripcion":"Agregar Entrevistado",
            "Entrevista":CodigoEntrevista,
            "listado":this.vector
        }
      ]

      //alert(JSON.stringify(Encabezado));
      let header:any =new HttpHeaders({'Content-Type':'application/json'});
      let url='http://rhmapp.000webhostapp.com/GestionEntrevista/ControllerEntrevista.php';
      this.http.post(url,JSON.stringify(Encabezado),header).
      subscribe(    
      (data):any=>{

     
      if(data[0].NumRegistro==this.numeroVoluntarios())
      {
        //alert("Consulta Exitosa");
        this.cargarPreguntaPorEntrevista(CodigoEntrevista);
      }

    }
   )




  }



  cargarPreguntaPorEntrevista(CodigoEntrevista)
  {
     let dato=[

      {
        "operacion":7,
        "descripcion":"cargar Preguntar por Entrevista",
        "Entrevista":CodigoEntrevista
      },

     ]
  
     let header:any =new HttpHeaders({'Content-Type':'application/json'});
     let url='http://rhmapp.000webhostapp.com/GestionEntrevista/ControllerEntrevista.php';
     this.http.post(url,JSON.stringify(dato),header).
     subscribe(
      (data):any=>{
        if(data[0].Respuesta=="Exito")
        {
          //alert("Fueron generados todos los preguntas por entrevista")
          this.showAlertExito();
         
        }
        else{
          this.showAlertFalla();
        }
      }
     )
   


  }


  numeroVoluntarios()
  { 
    let contador:number=0;
    for(let k in this.entrevistados)
    {
       contador++;
    }

    return contador;
  }

  showAlertExito()
  {
    let alert=this.alertControl.create(
      {
        title:'Agregar Huerta',
        subTitle:'Los datos fueron guardados',
        buttons:['ok']
      }
    );

    alert.present();
    this.FormEntrevista.reset();
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }


  showAlertFalla()
  {
    let alert=this.alertControl.create(
      {
        title:'Agregar Huerta',
        subTitle:'Los datos no fueron guardados',
        buttons:['ok']
      }
    );

    alert.present();

  }




}
