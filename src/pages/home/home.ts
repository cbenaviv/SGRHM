import { Component } from '@angular/core';
import { NavController,AlertController } from 'ionic-angular';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';
import { Http,Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import{Geolocation, Geoposition } from '@ionic-native/geolocation';
import {Camera,CameraOptions} from '@ionic-native/camera';
import { ImageViewerController } from 'ionic-img-viewer';
declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  FormHuerta:FormGroup;
  flag=0;
  valuePosition="";
  image: string = null;
  map:any;
  _imageViewerCtrl: ImageViewerController;
  constructor(public navCtrl: NavController,private fb:FormBuilder,public http:HttpClient,
  private alertControl:AlertController,private geolocation:Geolocation,private camara:Camera,
  imageViewerCtrl: ImageViewerController) {
    this._imageViewerCtrl = imageViewerCtrl;
    this.crearFormulario();
   
  }

  crearFormulario()
  {
    
    this.FormHuerta=this.fb.group(

      {
         huerta:['',Validators.required],
         ubicacion:['',Validators.required],
         Area:['',Validators.required],
         //Foto:['',Validators.required]
        
         
      }
 
      )

  }


  guardarHuerta()
  {
    
     let dato=[
     {
        "operacion":0,
         "Descripcion":'Insertar Huerta',
         "NombreHuerta":this.FormHuerta.value.huerta,
          "Ubicacion":this.FormHuerta.value.ubicacion,
          "AreaHuerta":this.FormHuerta.value.Area,
          "fotoHuerta":""
     },
    ]
   

    let header:any =new HttpHeaders({'Content-Type':'application/json'});
    let url='http://rhmapp.000webhostapp.com/GestionHuerta/controllerHuerta.php';
    this.http.post(url,JSON.stringify(dato),header).
    subscribe((data:any)=>
  {   
     
     
     //alert(data['_body']);
     this.showAlertExito();
  },
  (error:any)=>
  {
    //this.showAlertFalla();
  }


)
    



  }


  showAlertExito()
  {
    let alert=this.alertControl.create(
      {
        title:'Agregar Huerta',
        subTitle:'Los datos fueron guardados',
        buttons:['ok']
      }
    );

    alert.present();
    this.FormHuerta.reset();
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }

  showAlertFalla()
  {
    let alert=this.alertControl.create(
      {
        title:'Agregar Huerta',
        subTitle:'Los datos no fueron guardados',
        buttons:['ok']
      }
    );

    alert.present();

  }

  getPosition():any{
  
  }

  getPositionGeo()
  { 
    this.flag=1;
    this.geolocation.getCurrentPosition().then(response => {
      let latitude = response.coords.latitude;
      let longitude =response.coords.longitude;
       this.valuePosition=latitude+","+longitude;
       
     })
     .catch(error =>{
       console.log(error);
     })

    
    

  }

  
  
  
  getPicture()
  {
    this.flag=1;
    
    let options: CameraOptions = {
      destinationType: this.camara.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000
     
      
    }
    this.camara.getPicture( options )
    .then(imageData => {
      this.image = `data:image/jpeg;base64,${imageData}`;
     
    })
    .catch(error =>{
      console.error( error );
    });


  

  }


  seleccionarFoto(){
   
    this.flag=1;
     this.camara.getPicture({
      destinationType: this.camara.DestinationType.DATA_URL,
      sourceType: this.camara.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 1000,
      targetHeight: 1000
  }).then((imagen) => {
    console.log(imagen);
      this.image = "data:image/jpeg;base64," + imagen;
  }, (err) => {
      console.log(err);
  });        
  
  }

  presentImage(myImage) {
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();

   
  }
  







}
