import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EntrevistaPage } from './entrevista';

@NgModule({
  declarations: [
    EntrevistaPage,
  ],
  imports: [
    IonicPageModule.forChild(EntrevistaPage),
  ]
})
export class EntrevistaPageModule {}
