import { Component } from '@angular/core';
import { IonicPage, NavController,NavParams } from 'ionic-angular';
import {MenuaccesoPage} from '../menuacceso/menuacceso';

/**
 * Generated class for the EntrevistaPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-entrevista',
  templateUrl: 'entrevista.html'
})
export class EntrevistaPage {

  recientesRoot = 'RecientesPage'
  nuevaRoot = 'NuevaPage'
  listadoRoot = 'ListadoPage'
  menuPrincipalRoot = MenuaccesoPage;

  item;
  constructor(public navCtrl: NavController,public navParams: NavParams) {
    this.item=this.navParams.get("item");
  }

  myMethod()
  {
    this.navCtrl.setRoot(MenuaccesoPage,{item:this.item});
  }

}
