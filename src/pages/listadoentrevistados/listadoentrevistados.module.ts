import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListadoentrevistadosPage } from './listadoentrevistados';

@NgModule({
  declarations: [
    ListadoentrevistadosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListadoentrevistadosPage),
  ],
})
export class ListadoentrevistadosPageModule {}
