import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { Http,Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';


/**
 * Generated class for the ListadoentrevistadosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listadoentrevistados',
  templateUrl: 'listadoentrevistados.html',
})
export class ListadoentrevistadosPage {
  Entrevistados;
  info;
  j;
  vector=[];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public http:HttpClient,
    private alertControl:AlertController) {
    this.Entrevistados=navParams.data.entrevista;
  }

  ionViewDidLoad() {
    
    let dato=
    [
      {
        "operacion":3,
        "Descripcion": "Huerteros Entrevistados",
        "CodigoEntrevista":this.Entrevistados.idEntrevista
      },
    ]
    
   //alert(JSON.stringify(dato));
    
    let header:any =new HttpHeaders({'Content-Type':'application/json'});
    let url='http://rhmapp.000webhostapp.com/GestionEntrevista/ControllerEntrevista.php';
    this.http.post(url,JSON.stringify(dato),header).
    subscribe((data:any)=>
    {   this.info=data;
       
       
    
        
         for(this.j in data )
         {
          this.vector.push(data[this.j].Nombres+" "+data[this.j].Apellidos);  
          
         }

        
       
      
    }
    
  
  
  )

  }

}
