import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListarEntrevistaPage } from './listar-entrevista';

@NgModule({
  declarations: [
    ListarEntrevistaPage,
  ],
  imports: [
    IonicPageModule.forChild(ListarEntrevistaPage),
  ],
})
export class ListarEntrevistaPageModule {}
