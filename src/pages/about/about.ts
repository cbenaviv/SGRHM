import { Component } from '@angular/core';
import { NavController,AlertController } from 'ionic-angular';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';
import { ListahuertasProvider } from '../../providers/listahuertas/listahuertas';
import {CargarvoluntarioProvider} from '../../providers/cargarvoluntario/cargarvoluntario';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';


@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
   huertas;
   voluntario;
   FormVoluntarios:FormGroup;
  constructor(public navCtrl: NavController,public provedorLista:ListahuertasProvider,private fb: FormBuilder,
  private alertControl:AlertController,public provedorCargaVoluntario:CargarvoluntarioProvider,public http: Http) { 

     this.crearFormulario();
   }

  ionViewDidLoad()
  {
    this.provedorLista.obtenerDatos()
    .subscribe(
      (data)=>{this.huertas=data;},
      (error)=>{console.log(error);}
      
    )
  }

  crearFormulario()
  {
     this.FormVoluntarios=this.fb.group(

     {
        huerta:['',Validators.required],
        Nombres:['',Validators.required],
        Apellidos:['',Validators.required],
        Genero:['',Validators.required],
        Rol:['',Validators.required],
        Correo:[''],
        Telefono:['']
     }

     )
  }


  guardarVoluntario()
  {
     
    //alert (this.FormVoluntarios.value.Nombres);
    
    let datos=[
    {
      'operacion':3,
      'Descripcion':'Insertar Miembro',
      'CodigoHuerta':this.FormVoluntarios.value.huerta,
      'Nombres':this.FormVoluntarios.value.Nombres,
      'Apellidos':this.FormVoluntarios.value.Apellidos,
      'Genero':this.FormVoluntarios.value.Genero,
      'Correo':this.FormVoluntarios.value.Correo,
      'Telefono':this.FormVoluntarios.value.Telefono,
      'Rol':this.FormVoluntarios.value.Rol
    },
  ]
    let header:any =new HttpHeaders({'Content-Type':'application/json'});
    let url='http://rhmapp.000webhostapp.com/GestionHuerta/controllerHuerta.php';
    this.http.post(url,JSON.stringify(datos),header).
    subscribe((data:any)=>
  {   
     //alert(JSON.stringify(data))
     
     //alert(data['_body']);
     this.showAlertExito();
  },
  (error:any)=>
  {
    this.showAlertFalla();
  }


)


   

  

  
     
    
  
      


    

  }

  showAlertExito()
  {
    let alert=this.alertControl.create(
      {
        title:'Agregar Voluntario',
        subTitle:'Los datos fueron guardados',
        buttons:['ok']
      }
    );

    alert.present();
    this.FormVoluntarios.reset();
  }

  showAlertFalla()
  {
    let alert=this.alertControl.create(
      {
        title:'Agregar Voluntario',
        subTitle:'Los datos no fueron guardados',
        buttons:['ok']
      }
    );

    alert.present();

  }

 
}
