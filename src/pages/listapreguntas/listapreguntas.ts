import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {InfopreguntaPage} from '../infopregunta/infopregunta';


/**
 * Generated class for the ListapreguntasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listapreguntas',
  templateUrl: 'listapreguntas.html',
})
export class ListapreguntasPage {
   informacion;
   preguntas;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public http:HttpClient) {
    this.informacion=navParams.data.item
    
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ListapreguntasPage');
    let dato=[
      {
         "operacion":this.informacion.codigo, //Este valor varia del 1...5
          "Descripcion":this.informacion.titulo,
          "CodEntrevista":this.informacion.codEntrevista
           
      },
     ]

     //alert(JSON.stringify(dato));
     let header:any =new HttpHeaders({'Content-Type':'application/json'});
     let url='http://rhmapp.000webhostapp.com/GestionEntrevista/controllerPregunta.php';
     this.http.post(url,JSON.stringify(dato),header).
     subscribe((data)=>{this.preguntas=data

         //alert(JSON.stringify(data));
     }

     )
    
  }


  openNavDetailsPage(question)
  {
    this.navCtrl.push(InfopreguntaPage,{question:question,pregunta:this.preguntas});

  }

}
