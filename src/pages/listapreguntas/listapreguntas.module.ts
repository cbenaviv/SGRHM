import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListapreguntasPage } from './listapreguntas';

@NgModule({
  declarations: [
    ListapreguntasPage,
  ],
  imports: [
    IonicPageModule.forChild(ListapreguntasPage),
  ],
})
export class ListapreguntasPageModule {}
