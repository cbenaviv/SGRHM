import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListahuerterosPage } from './listahuerteros';

@NgModule({
  declarations: [
    ListahuerterosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListahuerterosPage),
  ],
})
export class ListahuerterosPageModule {}
