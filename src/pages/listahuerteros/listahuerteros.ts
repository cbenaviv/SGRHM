import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

/**
 * Generated class for the ListahuerterosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listahuerteros',
  templateUrl: 'listahuerteros.html',
})
export class ListahuerterosPage {
   codigoHuerta;
   j;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http:HttpClient) {
    this.codigoHuerta=navParams.data.huerta;

    let Solicitud=[
      {
        "operacion":4,
        "Descripcion":"Liste Todos los Miembros de La Huerta",
        "CodigoHuerta":this.codigoHuerta

      }
    ]

    //alert(JSON.stringify(Solicitud));
    this.listarHuerteros(Solicitud);
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListahuerterosPage');
    
  }


  listarHuerteros(Solicitud)
  {
    let header:any =new HttpHeaders({'Content-Type':'application/json'});
    let url='http://rhmapp.000webhostapp.com/GestionHuerta/controllerHuerta.php';
    this.http.post(url,JSON.stringify(Solicitud),header).
    subscribe(    
     (data):any=>{this.j=data
 
       
 
     }
    );
  }

}
