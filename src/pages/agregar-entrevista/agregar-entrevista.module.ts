import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgregarEntrevistaPage } from './agregar-entrevista';

@NgModule({
  declarations: [
    AgregarEntrevistaPage,
  ],
  imports: [
    IonicPageModule.forChild(AgregarEntrevistaPage),
  ],
})
export class AgregarEntrevistaPageModule {}
