import { Component } from '@angular/core';
import {IonicPage,NavController,NavParams} from 'ionic-angular';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import {MenuaccesoPage} from '../menuacceso/menuacceso';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
   item;
  constructor(public navCtrl:NavController,public navParams:NavParams) {
    this.item=this.navParams.get("item");
    //this.tab1Root.arguments(this.item);
  }

  myMethod()
  {
     this.navCtrl.setRoot(MenuaccesoPage,{item:this.item});
  }
}
